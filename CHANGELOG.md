# Changelog

<a name="0.1.0"></a>
## 0.1.0 (2021-03-26)

### Added

- 🔊 feat ajout log [[5352147](https://gitlab.com/anthonin.claude1/pics/commit/53521478f45fb9940f5dc3febf25ceb76db994fc)]
- 🎉 Initial Commit [[a0f04a1](https://gitlab.com/anthonin.claude1/pics/commit/a0f04a16d43c48c0ced9a2b8ee0b2b305bced3b4)]

### Miscellaneous

-  Merge branch &#x27;master&#x27; of https://gitlab.com/anthonin.claude1/pics [[efb3e31](https://gitlab.com/anthonin.claude1/pics/commit/efb3e31e027e70de98a933e624af462856053e6f)]
- 📝 feat modification du readme [[635fcba](https://gitlab.com/anthonin.claude1/pics/commit/635fcba19242dcaf1b048404f4c467b667d03f3c)]
- 🚀 Master Update .gitlab-ci.yml [[ef09401](https://gitlab.com/anthonin.claude1/pics/commit/ef09401aaf2beed45940c0118c6ede7e554ed7cc)]
- 🚀 Master Update .gitlab-ci.yml [[70710cd](https://gitlab.com/anthonin.claude1/pics/commit/70710cd5ddd308b8fccd919cbf9ad8e07bf815b4)]
-  :rocker: Master Update .gitlab-ci.yml [[2a5bfc4](https://gitlab.com/anthonin.claude1/pics/commit/2a5bfc4ae46b10047142f18bc84d22c7fa2bb1f9)]
- 🚀 Master Update .gitlab-ci.yml [[c6f954a](https://gitlab.com/anthonin.claude1/pics/commit/c6f954ac26b4ff43068fd3af9d55320fae705925)]
- 🚀 Master ajout fichier déploiement [[8d4dadf](https://gitlab.com/anthonin.claude1/pics/commit/8d4dadf5795f2dfa961514f76e850d765ec3f3a4)]
- 🛂 Changement clef API [[2a25922](https://gitlab.com/anthonin.claude1/pics/commit/2a25922fda4b070a073d2e847625ae832b75c864)]


