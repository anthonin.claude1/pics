This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Realisation du tp

Hormis pour les modifications du fichier .gitlab-ci.yml l'ensemble des modifications ont été effectué sur des branches feat puis mergé sur la branch develop.
Chaque commits est composé d’un gitmoji puis du type de branch sur lequel le commit est effectué.
J’ai créé trois applications sur Heroku, une pour chaque branche, dev, recette et prod. 
Ex pour prod : https://picsanthonin.herokuapp.com/
J'ai push les branches feat pour que vous poussiez les voir, même si je sais que normalement elle reste sur le pc du dev.
Pour la réalisation du fichier gitla-ci.yml je me suis aidé d’un tuto trouvé sur le site medium.com que vous trouverez ci dessous 
 
## Source 
https://docs.gitlab.com/ee/ci/quick_start/index.html
https://github.com/frinyvonnick/gitmoji-changelog
https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4
