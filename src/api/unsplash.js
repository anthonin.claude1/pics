import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID 158WpnW30j6LZGfqo9yy1_4S_bmcMqhPw9JCAp9k7Ow'
  }
});
